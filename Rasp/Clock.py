import time
import threading

class Clock :
    """ This class call back methods every 1ms
        To add your method juste type this line anywhere in your code:
            'clock = Clock()'
            'clock.addListene(your_method)'
    """
    
    class __impl :

        def __init__ (self, time) :
            
            self.time = time   # Set time between each impulsions
            self.resume = True  # Switch False to stop thread
            self.listeners = [] # contains listeners to call back

            #Start thread
            thread = threading.Thread(target=self.loop, args = [])
            thread.start()

            print("[Clock] Start clock with time={}".format(time))

        def loop (self) :
            while self.resume :
                time.sleep(self.time)
                self.updateListener()
            print("[Clock] stop clock")

        def addListener(self, listener) :
            self.listeners.append(listener)

        def deleteListener(self, listener) :
            self.listeners.remove(listener)

        def updateListener(self) :
            for listener in self.listeners :
                #Start listener in a new thread
                thread = threading.Thread(target=listener, args = [])
                thread.start()

        def stop(self) :
            self.resume = False


        def getId(self) :
            return id(self)


    instance = None

    def __init__(self, time=0.005) :
        if not Clock.instance :
            Clock.instance = Clock.__impl(time)

    def __getattr__(self, name) :
        return getattr(self.instance, name)

    def __setattr__(self, name) :
        return setattr(self.instance, name)
