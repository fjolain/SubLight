import numpy as np

class Network :
    """ Contains all network parts organized in three categories :
        - Line
        - Junction
        - EndLine
    """

    def __init__(self, n) :
        """ n : total length network """

        self.lines = []
        self.junctions = {}
        self.endlines = []
        self.M = np.ones((n,n))
        self.n = n
        self.max_length = 0

    def addLine(self, x, y) :
        """ Add Line on the network.
        
            x : start indice
            y : end indice
            The train go to x to y in clockwise direction
        """
        self.lines.append(Line(self.n, x, y))

    def addJunction(self, clockwise, junction_n, a, b, c=-1) :
        """ Add Junction on the network.
            
                       c
                      /
                     /
            a -------------- b    --> clockwise

            OR
                  c
                   \\
                    \\
            b -------------- a    --> clockwise

            If c stays at -1, it will be a two rail junction

            clockwise   : True if a-->b is on the clockwise direction
                          False if a-->b is NOT on the clockwise direction
            a, b, c     : indices led
            junction_n  : junction serial number
        """
        self.junctions[junction_n] = Junction(clockwise, self.n, a, b, c)

    def addEndLine(self, clockwise, increase, x) :
        """ Add a end line on the network.
            clockwise   : True if the train runs on clockwise direction goes to the end line.
                          False if the train runs on clockwise direction leaves the end line
            increase    : True if led indices are increased until the end. 
                          False if the led indices are decrease until the end
            x           : end line indice
        """
        self.endlines.append(EndLine(clockwise, increase, self.n, x))

    def updateDirection(self, direction) :
        # All parts are updated
        for line in self.lines :
            line.setDirection(direction)
        for junction in self.junctions.values() :
            junction.setDirection(direction)
        for endline in self.endlines :
            endline.setDirection(direction)

        self.computeMatrix()

    def updateSelectedWay(self, indice, way) :
        # Only junctions are uodated
        self.junctions[indice].setSelectedWay(way)
        self.computeMatrix()

    def updateLength(self, length) :
        # Only endline are updated
        for endline in self.endlines :
            endline.setLength(min(length, self.max_length))

        self.computeMatrix()

    def computeMatrix(self) :
        """Compute network matrix with curren direction, selected ways and length."""
        self.M = np.zeros((self.n, self.n))

        for line in self.lines :
            self.M = self.M + line.M
        for junction in self.junctions.values() :
            self.M = self.M + junction.M
        for endline in self.endlines :
            self.M = self.M + endline.M

    def isFreeJunction(self, train, junction_n=-1) :
        """ Return True if the train is not on a junction. False if the train is on a junction.
            train : vector train
            junction_n  : junction serial number, if -1 all junction are checked
        """
        # Gathering on which indices the train is
        train_indices = []
        for i in range(0, len(train)) :
            if train[i] == 1 :
                train_indices.append(i)

        # Gathering indices junctions wanted
        junction_indices = []
        if junction_n == -1 :
            for junction in self.junctions.values() :
                junction_indices.append(junction.a)
                junction_indices.append(junction.b)
                junction_indices.append(junction.c)

        else :
            junction_indices = [self.junctions[junction_n].a,
                                self.junctions[junction_n].b,
                                self.junctions[junction_n].c]
        for train_indice in train_indices :
            if train_indice in junction_indices :
                return False
        return True



class GlassHole :
    """ Represents a Generic glass hole where  [x] leds are sent to an [y] leds by a function.
        All circuit have only three glass hole type : Line, Junction and EndLine.
        The spefic function changes by train direction, selected way and length train.
        Don't use directly this object, use a daughter class.
    """ 

    def __init__(self, n, direction=True, way='b', length=3) :
        """ n           : total length circuit
            direction   : part direction. True : clockwise, False : anti-clockwise
            way         : only for junction. way selected between 'b' or 'c'
            length      : only for endline. train length
        """
        self.n = n
        self.direction = direction
        self.way = way
        self.length = length

    def updateMatrix(self) :
        """ Method to build the specific function describes as a matrix """
        pass

    def setDirection(self, direction):
        """ True    : clockwise direction
            False   : anti-clockwise direction
        """
        self.direction = direction
        self.updateMatrix()

    def setSelectedWay(self, way):
        """ 'b' : b way selected
            'c' : c way selected
        """
        self.way = way
        self.updateMatrix()

    def setLength(self, length) :
        """ length  : train length """
        self.length = length
        self.updateMatrix()

class Line (GlassHole) :
    """ Represents a strenght rail"""

    def __init__(self, n, x, y) :
        """ x : start indice
            y : end indice
            n : total circuit size
            The train go to x to y in clockwise direction
        """
        GlassHole.__init__(self, n)

        if x < y :
            self.increase = True
            self.x = x
            self.y = y
        else :
            self.increase = False
            self.x = y
            self.y = x

        self.updateMatrix()

    def updateMatrix(self) :
        # Set a empty matrice
        self.M = np.zeros((self.n, self.n))

        # Change direction if rail are decreasing
        if self.increase is False :
            self.direction = not self.direction
        
        if self.direction :    
            for i in range(self.x, self.y) :
                self.M[i+1, i] = 1

        else : 
            for i in range(self.x, self.y) :
                self.M[i, i+1] = 1


class Junction (GlassHole) :
    """ Represents a junction between three rails 
        Can be use for a junction between two rails
    """

    def __init__ (self, clockwise, n,  a, b, c=-1) :
        """ 
                       c
                      /
                     /
            a -------------- b    --> clockwise

            OR
                  c
                   \\
                    \\
            b -------------- a    --> clockwise

            If c stays at -1, it will be a two rail junction
            clockwise : True if a-->b is on the clockwise direction
                        False if a-->b is NOT on the clockwise direction
            n : total length circuit
        """

        GlassHole.__init__(self, n)
        self.a = a
        self.b = b
        if c ==-1 :
            self.c = b
        else :
            self.c = c
        
        self.clockwise = clockwise

        self.updateMatrix()

    def updateMatrix(self) :

        self.M = np.zeros((self.n, self.n))
        
        # Junction is on clockwise direction
        if self.clockwise :
            # Train is on clockwise direction
            if self.direction :
                # b is the selected way
                if self.way == 'b' :
                    self.M[self.b, self.a] = 1
                # c is the selected way
                if self.way == 'c' :
                    self.M[self.c, self.a] = 1

            # Train is on anti-clockwise direction
            else :
                self.M[self.a, self.b] = 1
                self.M[self.a, self.c] = 1

        # Junction is one clockwise direction
        else :
            # Train is on clockwise direction
            if self.direction :
                self.M[self.a, self.b] = 1
                self.M[self.a, self.c] = 1

            # Train is on anti-clockwise direction
            else :
                # b is the selected way
                if self.way == 'b' :
                    self.M[self.b, self.a] = 1
                # c is the selected way
                if self.way == 'c' :
                    self.M[self.c, self.a] = 1
                

class EndLine (GlassHole) :
    """ Represents the end of a line"""

    def __init__(self, clockwise, increase, n, x) :
        """ clockwise   : True if the train runs on clockwise direction goes to the end line.
                          False if the train runs on clockwise direction leaves the end line
            increase    : True if led indices are increased until the end. 
                          False if the led indeices are decrease until the end
            n           : total length circuit
            x           : end line indice
        """
        GlassHole.__init__(self, n)
        self.clockwise = clockwise
        self.increase = increase
        self.x = x

        self.updateMatrix()


    def updateMatrix(self) :
        
        # Set a empty matrice
        self.M = np.zeros((self.n, self.n))
        
        # If the train goes to the end line
        if self.clockwise == self.direction :
            
            # If the indices led are increased until the end line
            if self.increase :
                # A back loop moves the x position to x - length
                self.M[self.x- (self.length-1), self.x] = 1

            # If the indices led are decreases until the end line
            else :
                # A back loop moves the x postion to x + length
                self.M[self.x+ (self.length-1), self.x] = 1


