import socket
import threading
import select
import os


class Server :
    """ This class manage new connection and  new message from clients.
        It's build as a Singleton pattern.
    """

    class __impl :

        def __init__(self, port) :
            """ Begin server listening """

            # Clients to listening tab
            self.clients = []
            # Call back method tab
            self.listeners = []
            # stop endless loop when False
            self.resume = True

            self.connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.connection.bind(('', int(port) ))
            self.connection.listen(5)

            # launch new connection Thread
            thread_accept = threading.Thread(None, self.loopNewConnection, None, (), {})
            thread_accept.start()
            print("[Server] New connection loop started")
           
            # launch clients listening message thread
            thread_client = threading.Thread(None, self.loopClients, None, (), {})
            thread_client.start()
            print("[Server] Client listening loop started")

        def loopNewConnection(self) :
            while self.resume :
                server_update, w, e = select.select([self.connection], [], [], 0.05)
                for s in server_update :
                    # wait for a new connection
                    client, info = s.accept()
                    # add client in the list
                    self.clients.append(client)

            self.connection.close()
            print("[Server] Server loop stopped")


        def send2All(self, param) :
            """ Send a message to all client connected"""
            if type(param) == str :
                param += "\n"
                param = param.encode()
                for client in self.clients :
                    try :
                        client.send(param)
                    # If one exception rise client is deleted on all list
                    except :
                        self.clients.remove(client)

        def addClient(self, client) :
            """ Add Client to listening """
            self.clients.append(client)

        def deleteClient(self, client) :
            """ Delete Client to listening """
            if client in self.clients :
                self.clients.remove(client)

        def addListener(self, listener) :
            """ Add listener to catch new command """
            self.listeners.append(listener)

        def deleteListener(self, listener) :
            """ Delete listener to stop catching new command"""
            if listener in self.listeners :
                self.listeners.remove(listener)
    
        def update(self, client, tab) :
            """ call all listener with the new command"""
            for listener in self.listeners :
                # Start listener in a new thread
                thread = threading.Thread(target=listener, args = [client, tab])
                thread.start()
                #listener(client, tab)

        def loopClients(self) :
            """endless loop listening client connections """
            while self.resume :
                clients_update, wlist, xlist = select.select(self.clients, [], [], 0.05)
                for client in clients_update :
                    message = client.recv(1024)
                    # remove '\n' end char message
                    message = message.split('\n')[0]
                    # split by empty space
                    tab = message.split(' ')
                    #update listeners
                    self.update(client, tab)

            print("[Server] Client listening stopped")


        def stop(self) :
            self.resume = False

        def id(self) :
            return id(self)


    __instance = None

    def __init__(self, port=2016) :
        if Server.__instance is None :
            Server.__instance = Server.__impl(port)

    def __getattr__(self, attr) :
        return getattr(self.__instance, attr)
    def __setattr__(self, attr, value) :
        return setattr(self.__instance, attr, value)
