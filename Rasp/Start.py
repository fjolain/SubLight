#! /usr/bin/python

import json

from Network import *
#from TrainManager import *
#from Strip import Strip
#from Socket import Server
#from Clock import Clock


def readConfig(path) :

    # Get the json structure
    config_file = open(path, 'r')
    config = json.loads(config_file.read())

    # Extract the max led indice to find the number of led
    indices = []
    for line in config['line'] :    
        indices.append(line['start'])
        indices.append(line['end'])
    n = max(indices) +1 # Add one because index begins at 0
    
    # Construct network
    network = Network(n)

    # Extract lines
    for line in config['line'] :
        start = line['start']
        end = line['end']

        network.addLine(start, end)

    # Extract junctions
    for junction in config['junction'] :
        serial = junction['serial']
        a = junction['a']
        b = junction['b']
        c = junction['c']

        # Search if the junction is in clockwise
        # If 'a' is at the end of a line, junction is in clockwise
        clockwise = False
        for line in config['line'] :
            if a ==line['end'] :
                clockwise = True
        
        network.addJunction(clockwise, serial, a, b, c)

    # Extract end line
    for endline in config['end_line'] :
        position = endline['position']

        # Search if the endline is in clockwise
        # If 'position' is at the end of a line, endline is in clockwise
        clockwise = False
        for line in config['line'] :
            if position ==line['end'] :
                clockwise = True

        # Search if the end line follows increasing indices
        increase = True
        for line in config['line'] :
            start = line['start']
            end = line['end']
            if position == start or position == end :
                if clockwise :
                    increase = end > start

                else :
                    increase = end < start

        network.addEndLine(clockwise, increase, position)

    # Compute the max train length
    # The max train length is the minimum end line length
    max_length = 10000 
    for endline in config['end_line'] :
        
        # Search the line containing this end line
        for line in config['line'] :
            if endline['position'] == line['start'] or endline['position'] == line['end'] :
                max_length = min(max_length, abs(line['start']-line['end'])+1)

    network.max_length = max_length


    network.computeMatrix()

    return network, n, max_length



#Create network and Manager
network, n, max_length = readConfig('config.sbl')
#Start program components
#strip = Strip(n)
#clock = Clock(0.005) # launch interruption every 5ms
#server = Server(2016) # Listen on 2016 port
#manager = TrainManager(network)

