from neopixel import *
from Clock import *

class Strip :
    """ This class manage the Strip Led Neoled by the Adafruit Librairy
    """
    
    class __impl :

        def __init__ (self, n) :
            # LED strip configuration:
            LED_COUNT      = n      # Number of LED pixels.
            LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
            LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
            LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
            LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
            LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
            
            # Start strip
            self.strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
            self.strip.begin()

            # Create vetor
            self.vector = [None]*n
            self.n = n
            self.cleanVector()

            # self.clock = Clock()
            # self.clock.addListener(self.updateStrip)

        def cleanVector(self) :
            for i in range (0, self.n) :
                self.vector[i] = [0,0,0]
                
        def setVectorValue(self, indice, color) :
            self.vector[indice][0] += color[0]
            self.vector[indice][1] += color[1]
            self.vector[indice][2] += color[2]

        def updateStrip(self) :
            for indice in range (0, self.n) :
                self.strip.setPixelColorRGB(indice, self.vector[indice][0], self.vector[indice][1], self.vector[indice][2])
            self.strip.show()


        def getId(self) :
            return id(self)


    instance = None

    def __init__(self, n=20) :
        if not Strip.instance :
            Strip.instance = Strip.__impl(n)

    def __getattr__(self, name) :
        return getattr(self.instance, name)

    def __setattr__(self, name) :
        return setattr(self.instance, name)
