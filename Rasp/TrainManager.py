from Clock import *
from Network import *
from Strip import *
from Socket import *

import numpy as np
import copy
import time

class TrainManager :
    """ This class manage all trains in network.
        Each clock's call-back compute the next train vector.
        Each changement build a new matrix network
    """

    def __init__(self, network) :
        """ Start manager. Add updateTrains() as clock's listener and parser() as server's listener.
            - network : a network object containing all parts
        """

        self.clock = Clock()
        self.clock.addListener(self.updateTrains)

        self.server = Server()
        self.server.addListener(self.parser)

        self.strip = Strip()

        self.trains = {}

        self.network = network

        print("[TrainManager] Start")


    def addTrain(self, length) :
        """ Add a new train on the network.
            - length : train's length
            return the serial number attributed
        """

        # Fix a serial number. If first train then 0. Else max + 1
        serial = 0
        if self.trains != {} :
            serial = max(self.trains.keys()) + 1

        # Choose a random color
        R = np.random.randint(255)
        G = np.random.randint(255)
        B = np.random.randint(255)

        # Don't give the original network. Make a copy as like each train has this own network
        network_copy = copy.deepcopy(self.network)

        self.trains[serial] = Train(serial, length, [R, G, B], network_copy)
        return serial

    def deleteTrain(self, train_n) :
        """ Remove train from the network.
            - train_n : train serial number
        """
        if train_n in self.trains.keys() :
            del self.trains[train_n]

    def updateTrains(self) :
        """ Compute new vectors for each trains and send it to strip"""
        self.strip.cleanVector()
        for train in self.trains.values() :
            train.computeNextStep()
            for i in range(0, len(train.V)) :
                if train.V[i] == 1 :
                    self.strip.setVectorValue(i, train.color)

        self.strip.updateStrip()

    def setDirection(self, train_n, direction) :
        """ Set direction of train. 
            train_n     : train serial number 
            direction   : True : clockwise, False : anti-clockwise.
        """
        if train_n in self.trains.keys() :
            self.trains[train_n].setDirection(direction)


    def setSelectedWay(self, train_n, junction_n, way) :
        """ Set selected junction way.
            train_n     : train serial number
            junction_n  : junction serial number
            way         : way selected : 'b' or 'c'
        """
        if train_n in self.trains.keys() :
            self.trains[train_n].setSelectedWay(junction_n, way)

    def setLength(self, train_n, length) :
        """ Set Length train
            train_n : train serial number
            length  : new train's length
        """
        if train_n in self.trains.keys() :
            self.trains[train_n].setLength(length)

    def setColor(self, train_n, color) :
        """ Set the color's train
            - train_n : train serial number
            - color : [R, G, B] tab color
        """
        if train_n in self.trains.keys() :
            self.trains[train_n].setColor(color)

    def setSpeed(self, train_n, speed) :
        """ set the speed's train
            - train_n : train serial number
            - speed : 0 as stop to 10 as max
        """
        if train_n in self.trains.keys() :
            self.trains[train_n].setSpeed(speed)

    def stop (self) :
        self.clock.deleteListener(self.updateTrains)
        self.server.deleteListener(self.parser)

        self.clock.stop()
        self.server.stop()
        print("[TrainManager] Stop")

    
    def parser(self, client, ans) :
        """ Swap a tab command to an order for strip 
            - client : client socket which send command
            - ans : tab containing command
        """

        if ans[0] == 'exit' :
            self.stop()
            return None

        if ans[0] == "":
            return None
        
        # Check if the serial given is a number
        if len(ans) > 1 :
            try :
                int(ans[1])
            except ValueError :
                client.send('Serial given is not a number \n')

        # Select command
        
        if ans[0] == 'direction' :
            if len(ans) == 3 :
                if ans[2] == 't' :
                    self.setDirection(int(ans[1]), True)
                elif ans[2] == 'f':
                    self.setDirection(int(ans[1]), False)
                else :
                    client.send("wrong option \n")
            else :
                client.send("direction command takes 2 options \n")
    

        elif ans[0] == 'junction' :
            if len(ans) == 4 :
                if ans[3] == 'b' or ans[3] == 'c' :
                    try :
                        self.setSelectedWay(int(ans[1]), int(ans[2]), ans[3])
                    except ValueError :
                        client.send("serial junction must be a number \n")
                else :
                    client.send("wrong option\n")
            else : 
                client.send("junction command takes 3 options\n")

    
        elif ans[0] == 'color' :
            if len(ans) == 5 :
                try :
                    self.setColor(int(ans[1]), [int(ans[2]), int(ans[3]), int(ans[4]) ])
                except ValueError :
                    client.send("Wrong option\n")
            else :
                client.send("color command takes 4 options\n")

    
        elif ans[0] == 'speed' :
            if len(ans) == 3 :
                try :
                    self.setSpeed(int(ans[1]), int(ans[2]))
                except ValueError :
                    client.send("Wrong option\n")
            else :
                client.send("speed command takes 2 options\n")
    

        elif ans[0] == 'add' :
            if len(ans) == 2 :
                try :
                    client.send('new train {}\n'.format(self.addTrain(int(ans[1]))))
                except ValueError :
                    client.send("Wrong option\n")
            else :
                client.send("add command takes 1 option\n")


        elif ans[0] == 'delete' :
            if len(ans) == 2:
                self.deleteTrain(int(ans[1]))
            else :
                client.send("delete command takes 1 option\n")


        elif ans[0] == 'length' :
            if len(ans) == 3 :
                try :
                    self.setLength(int(ans[1]), int(ans[2]))
                except ValueError :
                    client.send("wrong option\n")
            else :
                client.send("length takes 2 options\n")
            
    
        else :
            client.send("command not found : {}\n".format(ans))


class Train :
    """ This class represents a train as :
        - a serial
        - a vector [0 0 ... 1 1 1 ... ]
        - an network
        - a set of color [R G B]
    """

    def __init__(self, serial, length, color, network) :
        """ Build the first vector """

        self.serial = serial
        self.length = length
        self.speed = 5 
        self.speed_iteration = 0 # Mute variable to count clock interruption
        self.color = color
        self.network = network

        # Update length
        self.network.updateLength(length)

        # Build first vector
        self.V = np.zeros(self.network.n)
        for i in range (0, self.length) :
            self.V[i] = 1

    
    def computeNextStep(self) :
        """ Compute the next Vector according to the network seting and speed train"""

        if self.speed != 0 :
            self.speed_iteration -= 1

            if self.speed_iteration <= 0 :
                self.V = np.dot(self.network.M, self.V)
                if self.speed == 10 :
                    self.speed_iteration = 0
                elif self.speed == 9 :
                    self.speed_iteration = 1
                elif self.speed == 8 :
                    self.speed_iteration = 2
                elif self.speed == 7 :
                    self.speed_iteration = 3
                elif self.speed == 6 :
                    self.speed_iteration = 4
                elif self.speed == 5 :
                    self.speed_iteration = 7
                elif self.speed == 4 :
                    self.speed_iteration = 10
                elif self.speed == 3 :
                    self.speed_iteration = 25
                elif self.speed == 2 :
                    self.speed_iteration = 50
                elif self.speed == 1 :
                    self.speed_iteration = 100


    def setDirection(self, direction) :
        """ Set direction of train. 
            direction   : True : clockwise, False : anti-clockwise.
        """
        # Wait junctions are free
        while not self.network.isFreeJunction(self.V) :
            time.sleep(0.01)

        self.network.updateDirection(direction)

    def setSelectedWay(self, junction_n, way) :
        """ Set selected junction way.
            junction_n  : junction serial number
            way         : way selected : 'b' or 'c'
        """
        # Wait junctions are free
        while not self.network.isFreeJunction(self.V) :
            time.sleep(0.01)

        self.network.updateSelectedWay(junction_n, way)

    def setLength(self, length) :
        """ Set Length train
            train_n : train serial number
            length  : new train length
        """
        # Wait junctions are free
        while not self.network.isFreeJunction(self.V) :
            time.sleep(0.01)

        self.length = length
        self.network.updateLength(length)

        # Create a new vector at the same position with new length
        position = 0
        while(self.V[position] == 0 and position <= len(self.V)-1) :
            position += 1

        # If the train is on a end line with indice increase, train will be shorted
        # We have to translate the position
        endlines_warning = []
        for endline in self.network.endlines :
            if endline.increase :
                endlines_warning.append(endline.x)

        for i in range (position, position+self.length) :
            if i in endlines_warning :
                position -= (position+self.length) -i
                break

        self.V = np.zeros(self.network.n)
        for i in range (position, position+self.length) :
            self.V[i] = 1

                

    def setColor(self, color) :
        """ Set the color's train
            - train_n : train serial number
            - color : [R, G, B] tab color
        """
        self.color = color

    def setSpeed(self, speed) :
        """ set the speed's train
            - train_n : train serial number
            - speed : 0 as stop to 10 as max
        """
        self.speed = speed
        self.speed_iteration = 0

