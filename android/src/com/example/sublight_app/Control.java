package com.example.sublight_app;

import socket.ConnectionSocket;
import socket.SocketListener;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

public class Control extends Activity {

	private TextView text;
	private SeekBar speed;
	private CheckBox direction;
	private SeekBar length;
	private Spinner color;
		
	private RadioGroup j1;
	private RadioGroup j2;
	private RadioGroup j3;
	private RadioGroup j4;
	private RadioGroup j5;
	private RadioGroup j6;
	private RadioGroup j7;
	
	private Train trainSelected;
	
	private ConnectionSocket socket;
	private String ip = "192.168.1.1";
	private int port = 2016;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_control);
		
		// Allow network
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		
		// Use landscape
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		// Instancie component from R file
		text = (TextView) findViewById(R.id.text);
		speed = (SeekBar) findViewById(R.id.speed);
		direction = (CheckBox) findViewById(R.id.direction);
		length = (SeekBar) findViewById(R.id.length);
		color = (Spinner) findViewById(R.id.color);
		j1 = (RadioGroup) findViewById(R.id.j1);
		j2 = (RadioGroup) findViewById(R.id.j2);
		j3 = (RadioGroup) findViewById(R.id.j3);
		j4 = (RadioGroup) findViewById(R.id.j4);
		j5 = (RadioGroup) findViewById(R.id.j5);
		j6 = (RadioGroup) findViewById(R.id.j6);
		j7 = (RadioGroup) findViewById(R.id.j7);
		
		// Fetch data from list view and train manager
		Intent intent = this.getIntent();
		int serial = intent.getIntExtra("sublight.main", 0);
		trainSelected = TrainManager.getInstance().getTrain(serial);
		
		// Update Components
		text.setText("Train n°"+String.valueOf(trainSelected.getSerial()));
		text.setBackgroundColor(trainSelected.getColorInt());
		speed.setProgress(trainSelected.getSpeed());
		direction.setChecked(trainSelected.isDirection());
		length.setProgress(trainSelected.getLength());
		color.setSelection(trainSelected.getColorPosition());
		
		//Update Junction
		if(trainSelected.getJunctions(1)=='b') {
			RadioButton rb = (RadioButton) findViewById(R.id.j1b);
			rb.setChecked(true);
		} else if (trainSelected.getJunctions(1)=='c') {
			RadioButton rb = (RadioButton) findViewById(R.id.j1c);
			rb.setChecked(true);
		}
		if(trainSelected.getJunctions(2)=='b') {
			RadioButton rb = (RadioButton) findViewById(R.id.j2b);
			rb.setChecked(true);
		} else if (trainSelected.getJunctions(2)=='c') {
			RadioButton rb = (RadioButton) findViewById(R.id.j2c);
			rb.setChecked(true);
		}
		if(trainSelected.getJunctions(3)=='b') {
			RadioButton rb = (RadioButton) findViewById(R.id.j3b);
			rb.setChecked(true);
		} else if (trainSelected.getJunctions(3)=='c') {
			RadioButton rb = (RadioButton) findViewById(R.id.j3c);
			rb.setChecked(true);
		}
		if(trainSelected.getJunctions(4)=='b') {
			RadioButton rb = (RadioButton) findViewById(R.id.j4b);
			rb.setChecked(true);
		} else if (trainSelected.getJunctions(4)=='c') {
			RadioButton rb = (RadioButton) findViewById(R.id.j4c);
			rb.setChecked(true);
		}
		if(trainSelected.getJunctions(5)=='b') {
			RadioButton rb = (RadioButton) findViewById(R.id.j5b);
			rb.setChecked(true);
		} else if (trainSelected.getJunctions(5)=='c') {
			RadioButton rb = (RadioButton) findViewById(R.id.j5c);
			rb.setChecked(true);
		}
		if(trainSelected.getJunctions(6)=='b') {
			RadioButton rb = (RadioButton) findViewById(R.id.j6b);
			rb.setChecked(true);
		} else if (trainSelected.getJunctions(6)=='c') {
			RadioButton rb = (RadioButton) findViewById(R.id.j6c);
			rb.setChecked(true);
		}
		if(trainSelected.getJunctions(7)=='b') {
			RadioButton rb = (RadioButton) findViewById(R.id.j7b);
			rb.setChecked(true);
		} else if (trainSelected.getJunctions(7)=='c') {
			RadioButton rb = (RadioButton) findViewById(R.id.j7c);
			rb.setChecked(true);
		}
		// Setup Raspberry connection by a socket
		socket = ConnectionSocket.getInstance(ip, port);
		socket.addSocketListener(new SocketListener(){

			@Override
			public void updateSocketListener(String str) {
				String[] tab = str.split(" ");
				
				if (tab[0].equals("param")) {
					int maxLength = Integer.parseInt(tab[1]);
					//int NbrJunction = Integer.parseInt(tab[2]);
					
					int tmp = trainSelected.getLength()-1;
					//length.setMax(maxLength-1);
					length.setProgress(tmp);
				}
				
				
				
			}
			
		});
		socket.send("init");
		
		
		// Setup speed
		speed.setMax(10);
		speed.incrementProgressBy(1);
		speed.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if (trainSelected != null)
					trainSelected.setSpeed(arg1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		// Setup direction
		direction.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				trainSelected.setDirection(arg1);
			}
			
		});
		
		// Setup length 
		length.setMax(4);
		length.incrementProgressBy(1);
		length.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if(trainSelected != null)
					trainSelected.setLength(arg1+1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		// Setup color
		color.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (trainSelected != null) {
					trainSelected.setColor(String.valueOf(arg0.getItemAtPosition(arg2)), arg2);
					text.setBackgroundColor(trainSelected.getColorInt());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		// Setup radiogroup
		JunctionsListener junctionsListener = new JunctionsListener();
		j1.setOnCheckedChangeListener(junctionsListener);
		j2.setOnCheckedChangeListener(junctionsListener);
		j3.setOnCheckedChangeListener(junctionsListener);
		j4.setOnCheckedChangeListener(junctionsListener);
		j5.setOnCheckedChangeListener(junctionsListener);
		j6.setOnCheckedChangeListener(junctionsListener);
		j7.setOnCheckedChangeListener(junctionsListener);
		
		
	}
		
	/*@Override
	protected void onResume() {
		super.onRestart();
		socket.connect();
	}*/
	
	private class JunctionsListener implements RadioGroup.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(RadioGroup arg0, int id) {
			
			int index = 0;
			char way = 'b';
			
			switch(id) {
			
			case R.id.j1b :
				index = 1;
				way = 'b';
				break;
			case R.id.j1c :
				index = 1;
				way = 'c';
				break;
				
			case R.id.j2b :
				index = 2;
				way = 'b';
				break;
			case R.id.j2c :
				index = 2;
				way = 'c';
				break;
				
			case R.id.j3b :
				index = 3;
				way = 'b';
				break;
			case R.id.j3c :
				index = 3;
				way = 'c';
				break;
				
			case R.id.j4b :
				index = 4;
				way = 'b';
				break;
			case R.id.j4c :
				index = 4;
				way = 'c';
				break;
				
			case R.id.j5b :
				index = 5;
				way = 'b';
				break;
			case R.id.j5c :
				index = 5;
				way = 'c';
				break;
				
			case R.id.j6b :
				index = 6;
				way = 'b';
				break;
			case R.id.j6c :
				index = 6;
				way = 'c';
				break;
				
			case R.id.j7b :
				index = 7;
				way = 'b';
				break;
			case R.id.j7c :
				index = 7;
				way = 'c';
				break;
			
			}
			
			
			Log.v("Debug", String.valueOf(index)+":"+way);
			if (trainSelected != null)
				trainSelected.setJunctions(index, way);
		}
		
	}
}
