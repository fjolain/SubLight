package com.example.sublight_app;

import java.util.ArrayList;

import socket.ConnectionSocket;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity {
		
	private ListView trains;
	private Button add;
	
	private AlertDialog.Builder ask;
	
	private ArrayAdapter<Train> adapter;
	private TrainManager singleton_trains;
	private	Train trainSelected;
	private int trainNumber = 0;
	private View viewSelected;
	
	private ConnectionSocket socket;
	private String ip = "192.168.1.1";
	private int port = 2016;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Start singleton train manager
		singleton_trains = TrainManager.getInstance();
		
		// Allow network
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		
		// Use landscape
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		// Instancie component from R file
		trains = (ListView) findViewById(R.id.trains);
		add = (Button) findViewById(R.id.add);						
		
		// Setup trains
		ArrayList<Train> list = new ArrayList<Train>();
		adapter = new ArrayAdapter<Train>(this, android.R.layout.simple_list_item_1, android.R.id.text1, list);
		trains.setAdapter(adapter);
		trains.setLongClickable(true);
		trains.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
				trainSelected = (Train) trains.getItemAtPosition(arg2);
				viewSelected = view;
				launchControl();				
			}
			
		});
		
		trains.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				trainSelected = (Train) trains.getItemAtPosition(arg2);
				ask.setMessage("Voulez supprimer le tain n°"+trainSelected.getSerial());
				ask.show();
				return true;
			}
			
		});
		
		
		// Setup add
		add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Train train = new Train();
				adapter.add(train);
				singleton_trains.addTrain(train);
				trainNumber++;
				
			}
			
		});
				
		// Setup alert dialog
		ask = new AlertDialog.Builder(this);
		ask.setTitle("Suppresion");
		ask.setPositiveButton("Oui", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				adapter.remove(trainSelected);
				singleton_trains.remove(trainSelected);
				trainNumber--;
				trainSelected = null;
			}
			
		});
		
		// Setup server connection
		ConnectionSocket.getInstance(ip, port);
		
	}
	
	
	public void launchControl() {
		Intent intent = new Intent(this, Control.class);
		intent.putExtra("sublight.main", trainSelected.getSerial());
		startActivity(intent);
	}
	
		
	@Override
	public void onResume() {
		super.onResume();
		// Setup Raspberry connection by a socket
		socket = ConnectionSocket.getInstance(ip, port);
		socket.connect();
		
		if(viewSelected != null)
			viewSelected.setBackgroundColor(trainSelected.getColorInt());
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		// Remove all train
		singleton_trains.removeAll();
	}
	
}


