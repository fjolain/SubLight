package com.example.sublight_app;

import socket.ConnectionSocket;
import socket.SocketListener;
import android.graphics.Color;
import android.util.Log;

public class Train {
	
	
	private int serial;
	private int length;
	private int speed;
	private String color;
	private int colorPosition;
	private boolean direction;
	
	private char[] junctions;
	
	// variable to wait the server responce with serial number
	private boolean waitingSerial;
	private int[][] colorRGB = {{  0,   0, 255}, // blue
								{255,   0,   0}, // red
								{  0, 255,   0}, // green
								{255, 165,   0}, // orange
								{255, 255,   0}, // yellow
								{123,  63,   0}, // brown
								{  0, 255, 255}, // cyan
								{255,   0, 255}, // magenta
								{ 75,   0, 130}, // indigo
								{112,  41,  99}};
	
	
	private ConnectionSocket socket;
	
	public Train() {
		
		
		
		// Default values according to the Raspberry pi program
		length = 5;
		serial = 1;
		speed = 5;
		color = "blue";
		colorPosition = 0;
		direction = true;
		junctions = new char[7];
		for(int i=0; i<junctions.length; i++)
			junctions[i] = 'b';
		
		
		// Add Socket listener
		socket = ConnectionSocket.getInstance();
		socket.addSocketListener(new SocketListener() {

			@Override
			public void updateSocketListener(String str) {
				String[] tab = str.split(" ");
				
				if (tab[0].equals("serial") && waitingSerial) {
					serial = Integer.parseInt(tab[1]);
					waitingSerial = false;
				}
				
			}
			
		});
		waitingSerial = true;
		socket.send("add "+String.valueOf(length));
		
		// Wait  for the server responce
		while(waitingSerial);
		
		
	}
	
	public String toString() {
		return "Train n°"+String.valueOf(serial);
	}
	
	/**
	 * @return the serial
	 */
	public int getSerial() {
		return serial;
	}


	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(int length) {
		this.length = length;
		
		String str = "length "+String.valueOf(serial)+" "+String.valueOf(length);
		socket.send(str);
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @param speed the speed to set [0 10](fore) [11 21](back)
	 */
	public void setSpeed(int speed) {
		this.speed = speed;	
		String str = "speed "+String.valueOf(serial)+" "+String.valueOf(this.speed);
		socket.send(str);
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	
	public int getColorInt() {
		return Color.rgb(colorRGB[colorPosition][0], colorRGB[colorPosition][1], colorRGB[colorPosition][2]);
	}
	
	/**
	 * @return the color position in spinner
	 */
	public int getColorPosition() {
		return colorPosition;
	}

	/**
	 * @param color the color to set
	 * @param colorPosition color position in spinner
	 */
	public void setColor(String color, int colorPosition) {
		this.color = color;
		this.colorPosition = colorPosition;
		
		String str = "color "+String.valueOf(serial)+" "+String.valueOf(colorRGB[colorPosition][0])
													+" "+String.valueOf(colorRGB[colorPosition][1])
													+" "+String.valueOf(colorRGB[colorPosition][2]);
		socket.send(str);
	}

	/**
	 * @return the direction
	 */
	public boolean isDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(boolean direction) {
		this.direction = direction;
		
		char d;
		if (direction)
			d = 't';
		else
			d ='f';
		String str = "direction "+String.valueOf(serial)+" "+d;
		socket.send(str);
		
	}

	/**
	 * @param index junction serial number
	 * @return the junctions
	 */
	public char getJunctions(int index) {
		return junctions[index-1];
	}

	/**
	 * @param index junction serial number
	 * @param way selected way 'b' or 'c'
	 */
	
	public void setJunctions(int index, char way) {
		if (way=='c' || way=='b')
			junctions[index-1] = way;
		
		String str ="junction " + String.valueOf(serial)
							+" "+ String.valueOf(index)
							+" "+ way;
		socket.send(str);
	}
	
	/**
	 * Delete train on server
	 */
	public void delete() {
		String str = "delete "+String.valueOf(serial);
		socket.send(str);
	}

}
