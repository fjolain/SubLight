package com.example.sublight_app;

import java.util.Hashtable;

import android.util.Log;

public class TrainManager {
	
	private Hashtable<Integer, Train> trains = new Hashtable<Integer, Train>();
	private static TrainManager instance;
	
	private TrainManager() {
		
	}
	
	public static TrainManager getInstance() {
		if (instance == null)
			instance = new TrainManager();
		return instance;
	}
	
	public void addTrain(Train train) {
		trains.put(train.getSerial(), train);
	}
	
	public Train getTrain(int serial) {
		return trains.get(serial);
	}
	
	public void remove(Train train) {
		trains.remove(train);
		train.delete();
	}
	
	public void removeAll() {
		for(Train train : trains.values())
			remove(train);
	}

}
